# Dockerfile com PHP e nginx #

Como criar um projeto php-nginx dockerizado pronto para produção.

### Pré requisitos ###

* [Docker](https://docs.docker.com/get-docker/)
* [Docker compose](https://docs.docker.com/compose/install/)

### Iniciando ###

O Dockerfile deve ser criado na raiz do projeto

Usaremos a seguinte imagem para buildar o projeto
 - [webdevops-php-nginx](https://dockerfile.readthedocs.io/en/latest/content/DockerImages/dockerfiles/php-nginx.html#webdevops-php-nginx)

### Dockerfile ###

Para começar, usaremos o comando “FROM” e depois, a imagem da onde nós vamos nos basear para criar nossa imagem citada acima junto com a versão do PHP. Nesse caso usaremos a 7.4.

```dockerfile
FROM webdevops/php-nginx:7.4
```

Com o comando “ENV”, definimos uma variável para o nosso document root apontando para a pasta public.

```dockerfile
ENV WEB_DOCUMENT_ROOT=/app/public
```

Vamos declarar o diretório principal do projeto dentro do container "WORKDIR".

```dockerfile
WORKDIR /app
```


Com o comando “RUN”, rodamos comandos dentro do nosso futuro container. 

```dockerfile
RUN docker-php-ext-install mysqli pdo pdo_mysql tokenizer xml pcntl
```
É importante o parâmetro -y no apt-get para que ele não te pergunte nada durante a instalação dos pacotes.
```dockerfile
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y libicu-dev g++
RUN composer install
```

Agora precisamos expor as portas para que o nosso host tenha acesso às portas do container.

```dockerfile
EXPOSE 80
```

O seu Dockerfile deve ficar parecido com isso:

```dockerfile
FROM webdevops/php-nginx:7.4

ENV WEB_DOCUMENT_ROOT=/app/public

WORKDIR /app

RUN docker-php-ext-install mysqli pdo pdo_mysql tokenizer xml pcntl

RUN apt-get install -y zlib1g-dev

RUN apt-get install -y libicu-dev g++

RUN composer install

EXPOSE 80
```